package delta.controller;

import EmailService.Email;
import db.DataBase;
import db.DropPointDB;
import db.NoDatabaseConnectionException;

import javax.mail.MessagingException;
import java.io.FileNotFoundException;
import java.sql.SQLException;

public class InfractionFormController {
    /**
     * DropPoint database.
     */
    private DropPointDB dropPointDB;
    /**
     * maintenance worker email.
     */
    private String maintenanceWorkerEmail;
    private String [][] droppoints;
    /**
     * Creates an instance of InfractionFormController and starts the database.
     * @param maintenanceWorkerEmail maintenance worker email.
     */
    public InfractionFormController(String maintenanceWorkerEmail) throws FileNotFoundException, NoDatabaseConnectionException {
        this.dropPointDB= new DropPointDB(new DataBase());
        this.maintenanceWorkerEmail=maintenanceWorkerEmail;
    }

    /**
     * returns all droppoints with the name, street and door.
     * @return all droppoints with the name, street and door.
     */
    public String [] getAllDropPoints() throws SQLException, NoDatabaseConnectionException {
        this.droppoints = dropPointDB.getAllDropPointsNameStreet();
        String []finalDropPoint = new String[droppoints.length];
        for (int i = 0; i < droppoints.length; i++) {
            finalDropPoint[i]=droppoints[i][1]+", "+droppoints[i][2]+", "+droppoints[i][3];
        }
        return finalDropPoint;
    }

    public boolean registerIncident(int dropPointIndex) throws NoDatabaseConnectionException, SQLException {
        return this.dropPointDB.insertIncident(dropPointIndex,this.maintenanceWorkerEmail);
    }
    /**
     * Sends an email to the police with the text.
     * @param from The company email.
     * @param to The police email.
     * @param subject The subject of the email.
     * @param text The text of the email.
     */
    public void sendEmail(String from, String to, String subject, String text) throws MessagingException{
        Email email = new Email();
        email.sendEmail(from, to, subject, text);
    }
}
