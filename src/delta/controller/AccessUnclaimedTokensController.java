package delta.controller;

import db.DataBase;
import db.DropPointDB;
import db.NoDatabaseConnectionException;
import db.TokenDB;
import utils.RandomTokenCodeGenerator;

import java.io.FileNotFoundException;

import java.sql.SQLException;
import java.util.Random;
import java.util.UUID;

public class AccessUnclaimedTokensController {
    /**
     * Token DataBase.
     */
    private TokenDB tokenDB;
    /**
     * Drop point DataBase.
     */
    private DropPointDB dropPointDB;
    /**
     * Maintenance Worker Email.
     */
    private String maintenanceWorkerEmail;

    /**
     * Creates an instance that initializes the database .
     * @param maintenanceWorkerEmail The maintenance worker email.
     */
    public AccessUnclaimedTokensController(String maintenanceWorkerEmail) throws FileNotFoundException, NoDatabaseConnectionException {
        this.maintenanceWorkerEmail= maintenanceWorkerEmail;
        this.tokenDB = new TokenDB(new DataBase());
        this.dropPointDB = new DropPointDB(new DataBase());
    }

    /**
     * Updates the existing tokens that were not used to inactive and creates new Unclaimed Packages token.
     */
    public void updateTokens() throws SQLException, NoDatabaseConnectionException {
        String [][] reservationID = this.tokenDB.getUnclaimedPackagesIDReservation();
        String fakePassword = UUID.randomUUID().toString().toLowerCase();
        this.tokenDB.updateOutOfExpirationDate();
        for (int i = 0; i < reservationID.length; i++) {
            RandomTokenCodeGenerator tcg = new RandomTokenCodeGenerator(fakePassword);
            String tokenDescription = tcg.generateCode();
            this.tokenDB.insertUnclaimedTokenIntoDB(reservationID[i][0],tokenDescription);
        }
    }

    /**
     * Returns the drop points of the course in which the maintenance worker is part of.
     * @return a list of drop point.
     */
    public String [][] getDropPointCourse() throws SQLException, NoDatabaseConnectionException {
        return this.dropPointDB.getDropPointMaintenanceCourse(this.maintenanceWorkerEmail);
    }

    /**
     * Returns the unclaimed packages token for the given drop point.
     * @param dropPointID The drop point id
     * @return The unclaimed packages token,
     */
    public String[] getUnclaimedPackagesToken(int dropPointID) throws SQLException, NoDatabaseConnectionException {
        return this.tokenDB.getUnclaimedPackagesToken(dropPointID)[0];

    }
}
