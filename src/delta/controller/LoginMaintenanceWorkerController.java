package delta.controller;


import db.DataBase;
import db.DropPointDB;
import db.EmptySelectException;
import db.NoDatabaseConnectionException;

import java.io.FileNotFoundException;
import java.sql.SQLException;

public class LoginMaintenanceWorkerController {
    /**
     * Object used to make the connection with the Drop Point related database methods.
     */
    private DropPointDB dropPointDB;

    public LoginMaintenanceWorkerController() throws FileNotFoundException, NoDatabaseConnectionException {
        dropPointDB=new DropPointDB(new DataBase());
    }


    /**
     * Get the email and password to compare with the insert data by the maintenance worker in the login process.
     * @param email
     * @return email and password
     */
    public String [] getMaintenanceDataByEmail(String email) throws EmptySelectException, SQLException, NoDatabaseConnectionException {
        return dropPointDB.getMaintenanceDataByEmail(email);
    }
}
